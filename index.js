/*
const express = require("express");

// app - server
const app = express();

const port = 3000;

// Middlewares - software that provides common services and capabilities to applications outside of what's offered by the operating system
// allows your app to read JSON data
app.use(express.json());

// allows your app to read data from any other forms
app.use(express.urlencoded({extended: true}));

// [SECTION] ROUTES
// GET
app.get("/greet",  (request, response) => {
	response.send("Hello from the /greet endpoint!");
});

// POST Method
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

// Simple registration form

let users = [];

app.post("/signup", (request, response) => {

	if (request.body.username !== '' && request.body.password !== '') {
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
	} else {
		response.send("Please input BOTH username and password.");
	}
});

// Simple change password transaction
app.patch("/change-password", (request, response) => {

	let message;

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username) {
			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated!`;
			break;
		} else {
			message = "User does not exist.";
		}
	}
	response.send(message);
});


app.listen(port, () => console.log(`Server running at ${port}`));

*/
// [SECTION] ACTIVITY STARTS HERE

const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}));

// /home
app.get("/home",  (request, response) => {
	response.send("Welcome to the home page");
});

// /items
let users = [];
app.get("/items",  (request, response) => {
	response.send(
	users = [
	{
		"username": "johndoe",
		"password": "johndoe1234"
	},
	{
		"username": "jimmyboi",
		"password": "jimmyboi1234"
	},
	]);
});

// DELETE
app.delete("/delete-user", (request, response) => {
	let message;

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username) {
			message = `User ${request.body.username} has been deleted.`;
			break;
		} else {
			message = `User does not exist.`;
		}
	}
	response.send(message);
});



app.listen(port, () => console.log(`Server running at ${port}`));

// [SECTION] ACTIVITY END HERE



























